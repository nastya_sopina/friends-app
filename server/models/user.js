const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');


const userSchema = new Schema({
  email : {type: String},
  check : {type: Boolean, default: false},
  username : {type: String, required: true, unique:true, lowercase: true},
  password : {type: String, required: true},
  fullname: {type: String, default: ''},
  phonenumber: {type: String, default: ''},
  country: {type: String, default: ''},
  city: {type: String, default: ''},
  website: {type: String, default: ''},
  friends : [
    {
      username: {type: String},
      fullname: {type: String},
    }
    ]
});

userSchema.pre('save', function(next){
  if (!this.isModified('password')){
    return next();
  }

  bcrypt.hash(this.password, null,null,(err,hash) => {
    if(err){
      return next(err);
    }
    this.password = hash;
    next();
  });

});

userSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password,this.password);
};

module.exports = mongoose.model('User', userSchema);
