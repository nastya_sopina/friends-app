const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');


const publicationSchema = new Schema({
  text : {type: String, required: true},
  user : {type: String, required: true},
  username : {type: String, required: true},
  createdAt : {type: Date, default: Date.now()},
  comments : [
    {
      comment: {type: String},
      commentator: {type: String},
      commentatorname: {type: String},
      comentatorusername : {type:String},
    }
    ]
});

publicationSchema.pre('save', function(next){
  if (!this.isModified('password')){
    return next();
  }

  bcrypt.hash(this.password, null,null,(err,hash) =>{
    if(err){
      return next(err);
    }
    this.password = hash;
    next();
  });

});


module.exports = mongoose.model('Publication', publicationSchema);
