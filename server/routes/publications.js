const User = require('../models/user');
const Publication = require('../models/publication')
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const api_key = 'key-83e9256d2a63a119906bac57d7e8191c';
const domain = 'sandboxe25cfef7ec334386acaa4ac7d03016ad.mailgun.org';
const mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

module.exports = (router) => {

  router.post('/newPublication',(req,res)=>{
    if(!req.body.text){
      res.json({success: false, message: 'Введите текст'})
    } else {
      if (!req.body.user){
        res.json({success: false, message: 'Не найден пользователь'})
      } else{
        let publication = new Publication({
          text: req.body.text,
          user: req.body.user,
          username: req.body.username,
        });

        publication.save((err)=>{
        if(err){
          res.json({success: false, message: 'Публикация не сохранена' + err})
        } else{
        res.json({success: true, message:'Публикация сохранена'})
      }
      });
    }
    }
  });

  router.get('/getAllPublications',( req, res)=>{
    Publication.find({},(err,publications)=>{
      if(err){
        res.json({success: false, message: err})
      } else{
        if (!res){
          res.json({success: false, message:'Публикация не найдена'})
        } else{
          res.json({success: true, publications: publications})
        }
      }
    }).sort({'_id': -1})
  });

  router.get('/editPost/:id',( req, res)=>{

    Publication.findOne({ _id: req.params.id},(err, publication) => {
      if(err){
        res.json({success: false, message: err})
      } else{
        if(!publication){
          res.json({success: false, message:'Публикация не найдена'})
        } else {
          res.json({success: true, publication: publication})
        }
      }
    })
  });

  router.put('/updatePost',(req, res) => {
    Publication.findOne({ _id: req.body._id},(err, publication) => {
      if(err){
        res.json({success: false, message: err})
      } else{
        if(!publication){
          res.json({success: false, message:'Публикация не найдена'})
        } else {
          publication.text = req.body.text;
          publication.save((err)=>{
            if(err){
              res.json({success: false, message:'Что-то пошло не так'})
            } else{
              res.json({success: true, message:'Публикация обновлена'})
            }
          })
        }
      }
    })
  });

  router.delete('/deletePost/:id',(req,res)=>{

    Publication.findOne({ _id: req.params.id},(err, publication) => {
      if(err){
        res.json({success: false, message: err})
      } else{
        if(!publication){
          res.json({success: false, message:'Публикация не найдена'})
        } else {
          publication.remove((err)=>{
            if(err) {
              res.json({success: false, message: err})
            } else {
              res.json({success: true, message: 'Публикация удалена'})
            }})
        }
      }
    })

  });

  router.post('/comment', (req, res) => {

    if (!req.body.comment) {
      res.json({success: false, message: 'Нет коментария'})
    } else {
      if (!req.body.id) {
        res.json({success: false, message: 'Нет id '})
      } else {

        Publication.findOne({_id: req.body.id}, (err, publication) => {
          if (err) {
            res.json({success: false, message: 'Неверное id публикации'})
          } else {
            if (!publication) {
              res.json({success: false, message: 'Публикация не найдена'})
            } else {

              User.findOne({_id: req.decoded.userId}, (err, user) => {
                if (err) {
                  res.json({success: false, message: 'Что-то пошло не так'})
                } else {
                  if (!user) {
                    res.json({success: false, message: 'Пользователь не найден'})
                  } else {

                    publication.comments.push({
                      comment: req.body.comment,
                      comentator: user.username,
                      commentatorname: user.fullname,
                      comentatorusername: user.username,
                    });


                    publication.save((err) => {
                      if (err) {
                        res.json({success: false, message: 'Что-то пошло не так'})
                      } else {
                       const textmes = 'Вашу запись с текстом:  ' + ' ' +publication.text + '  прокоментировал(а):  ' + user.fullname
                        const data = {
                          from: 'Nastya Sopina <sawirani@yandex.ru>',
                          to: user.email,
                          subject: 'Вашу запись прокоментировали',
                          text: textmes,
                        };

                        mailgun.messages().send(data, function (error, body) {
                          if (body) {
                            res.json({success: true, message: 'Комментарий сохранен'})
                          }
                        })
                      }
                    });
                  }
                }
              })

            }
          }
        });
      }
    }
  });

  return router;
};
