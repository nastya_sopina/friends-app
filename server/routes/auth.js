const User= require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const api_key = 'key-83e9256d2a63a119906bac57d7e8191c';
const domain = 'sandboxe25cfef7ec334386acaa4ac7d03016ad.mailgun.org';
const mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

module.exports = (router) => {

  router.post('/register', (req, res) =>{

    if (!req.body.email){
      res.json({success: false, message: 'Введите email'});
    } else {
      if (!req.body.username) {
        res.json({success: false, message: 'username уже занят'});
      } else {
        if (!req.body.password) {
          res.json({success: false, message: 'Пароль не верный'});
        } else {
          let users = new User({
            fullname: req.body.firstname + ' ' + req.body.secondname,
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
          });
          users.save((err)=> {
            if(err){
              if (err.code === 11000){
                res.json({success: false, message: 'Username или e-mail уже существует'});
              } else {
                res.json({success: false, message: 'Пользователь не зарегестрирован. Error:', err});
              }
              } else {

              const data = {
                from: 'Nastya Sopina <sawirani@yandex.ru>',
                to: users.email,
                subject: 'подтверждение email in friends',
                text: 'Введите этот код на сайте:' +  users._id,
              };

              mailgun.messages().send(data, function (error, body) {
              if(body){
                res.json({success: true,  message: 'Вы зарегестрированны! Войдите в систему и подтвердите свой e-mail' });
              }
              });
            }
          });

        }
      }
    }
  });

  router.get('/checkUsername/:username', (req, res) => {

    if (!req.params.username) {
      res.json({ success: false, message: 'Пользователь не найден' });
    } else {

      User.findOne({ username: req.params.username }, (err, user) => {

        if (err) {
          res.json({ success: false, message: err });
        } else {

          if (user) {
            res.json({ success: false, message: 'Username занят' });
          } else {
            res.json({ success: true, message: 'Username свободен' });
          }
        }
      });
    }
  });

  router.get('/checkEmail/:email', (req, res) => {

    if (!req.params.email) {
      res.json({ success: false, message: 'Email не верный' });
    } else {

      User.findOne({ email: req.params.email }, (err, user) => {

        if (err) {
          res.json({ success: false, message: err });
        } else {

          if (user) {
            res.json({ success: false, message: 'этот Email уже зарагестрирован' });
          } else {
            res.json({ success: true, message: 'Все в порядке' });
          }
        }
      });
    }
  });

  router.post('/login', (req, res) => {

    if (!req.body.username) {
      res.json({ success: false, message: 'Пользователь на найден' });
    } else {
      if (!req.body.password) {
        res.json({ success: false, message: 'Пароль не верный' });
      } else {
        User.findOne({ username: req.body.username.toLowerCase() }, (err, user) => {
          if (err) {
            res.json({ success: false, message: err }); // Return error
          } else {
            if (!user) {
              res.json({ success: false, message: 'Пользователь не найден.' });
            } else {
              const validPassword = user.comparePassword(req.body.password);
              if (!validPassword) {
                res.json({ success: false, message: 'Неверный пароль' });
              } else {
                const token = jwt.sign({ userId: user._id }, config.secret, { expiresIn: '24h' });
                res.json({ success: true, message: 'Вход выполнен!', token: token, user: { username: user.username } });
              }
            }
          }
        });
      }
    }
  });

  router.post('/login2', (req, res) => {

      if (!req.body.email) {
        res.json({ success: false, message: 'E-mail на найден' });
      } else {
        if (!req.body.password) {
          res.json({success: false, message: 'Пароль не верный'});
        } else {
          if (req.body.id) {
            User.findOne({_id: req.body.id}, (err, user) => {
              if (err) {
                res.json({success: false, message: err});
              } else {
                if (!user) {
                  res.json({success: false, message: 'Пользователь не найден'});
                } else {
                  const validPassword = user.comparePassword(req.body.password);
                  if (!validPassword) {
                    res.json({success: false, message: 'Неверный пароль'});
                  } else {
                    const token = jwt.sign({userId: user._id}, config.secret, {expiresIn: '24h'});
                    user.check = true;
                    user.save((err) => {
                      if (err) {
                        res.json({success: false, message: 'Что-то пошло не так..'})
                      } else {
                        res.json({
                          success: true,
                          message: 'Вход выполнен!',
                          token: token,
                          user: {email: user.email, username: user.username, check: user.check}
                        });
                      }
                    })
                  }
                }
              }
            });
          } else {
            User.findOne({email: req.body.email.toLowerCase()}, (err, user) => {
              if (err) {
                res.json({success: false, message: err}); // Return error
              } else {
                if (!user) {
                  res.json({success: false, message: 'Пользователь не найден.'});
                } else {
                  const validPassword = user.comparePassword(req.body.password);
                  if (!validPassword) {
                    res.json({success: false, message: 'Неверный пароль'});
                  } else {
                    const token = jwt.sign({userId: user._id}, config.secret, {expiresIn: '24h'});
                    res.json({success: true, message: 'Вход выполнен!', token: token, user: user});
                  }
                }
              }
            });
          }
        }
      }
    });

  router.use((req, res, next) => {
    const token = req.headers['authorization'];

    if (!token) {
      res.json({ success: false, message: 'Нет токена' });
    } else {
      jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
          res.json({ success: false, message: 'Неверный токен: ' + err });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    }
  });

  router.get('/profil/:num', (req, res) => {

    User.findOne({ _id: req.params.id },(err, user) => {
      if (err) {
        res.json({ success: false, message: err });
      } else {
        if (!user) {
          res.json({ success: false, message: 'Пользователь не найден' });
        } else {
          res.json({ success: true, user: user });
        }
      }
    });

  });

  router.get('/profile', (req, res) => {

    User.findOne({ _id: req.decoded.userId },(err, user) => {
      if (err) {
        res.json({ success: false, message: err });
      } else {
        if (!user) {
          res.json({ success: false, message: 'Пользователь не найден' });
        } else {
          res.json({ success: true, user: user });
        }
      }
    });

  });

  router.get('/profiles/:id', (req, res) => {

    User.findOne({ username: req.params.id},(err, user) => {
      if (err) {
        res.json({ success: false, message: err });
      } else {
        if (!user) {
          res.json({ success: false, message: 'Пользователь не найден' });
        } else {
          res.json({ success: true, user: user });
        }
      }
    });

  });

  router.put('/editOverview/:id',(req, res) => {

    User.findOne({_id: req.body._id}, (err, user) => {
      if (err) {
        res.json({success: false, message: err})
      } else {
        if (!user) {
          res.json({success: false, message: 'Пользователь не найден'})
        } else {

          if ((user.username === req.body.username) && (user.phonenumber === req.body.phonenumber) && (user.country === req.body.country) && (user.city === req.body.city)) {
            res.json({success: false, message: 'Ничего не изменилось'})
          } else {
            if (req.body.username) {
              user.username = req.body.username;
            }
            if (req.body.phonenumber) {
              user.phonenumber = req.body.phonenumber;
            }
            if (req.body.country) {
              user.country = req.body.country;
            }
            if (req.body.city) {
              user.city = req.body.city;
            }

            user.save((err) => {
              if (err) {
                res.json({success: false, message: 'Что-то пошло не так..'})
              } else {
                res.json({success: true, message: 'Информация обновленна'})
              }
            })
          }
        }
      }
    });
  });

  router.get('/allProfiles',(req,res) => {
    User.find({},(err, users) =>{
      if (err){
        res.json({success: false, message: 'Что-то пошло не так'});
      } else {
        if (!users){
          res.json({success: false, message: 'Пользователь не найден'});
        } else{
          res.json({success: true, users: users});
        }
      }
    })
  });

  router.get('/searchprofile/:text',(req,res) =>{

    User.find({},(err,users)=>{
      if (!users){
        res.json({ success: false, message: 'Что-то пошлоо не так' });
      } else {
        if (users){
          const searchtext = new RegExp('^' + req.params.text);
          let profiles = [];
          users.forEach(function (user) {
            let username = user.fullname.toLowerCase();
            if (!username.search(searchtext)) {
              profiles.push(user);
            }
          });
          res.json({success: true, users: profiles});
        }
      }
        });
  });

  router.post('/addFriends',(req,res) => {

    if (!req.body.user) {
      res.json({ success: false, message: 'No user was provided' });
    } else {
      if (!req.body.username) {
        res.json({ success: false, message: 'No username was provided' });
      } else {
        User.findOne({ username: req.body.username.toLowerCase() }, (err, friend) => {
          if (err) {
            res.json({ success: false, message: err });
          } else {
            if (!friend) {
              res.json({ success: false, message: 'Пользователь не найден' });
            } else {
              User.findOne({ username: req.body.user.toLowerCase() }, (err, user) => {
                if (err) {
                  res.json({ success: false, message: err });
                } else {
                  if (!user) {
                    res.json({ success: false, message: 'Пользователь не найден' });
                  } else {
                    user.friends.push(
                      {
                        username: friend.username,
                        fullname: friend.fullname,
                      }
                      );

                    user.save((err)=>{
                      if(err){
                        res.json({success: false, message: 'Что-то пошло не так...'})
                      } else{
                        res.json({success: true, message: 'Пользователь сохранен'})
                      }
                    })
                  }
                }
              });

            }
          }
        });
      }
    }

  });

  router.get('/checkLogin/:id', (req,res) => {
    User.find({username: req.params.username},(err, user) =>{
      if (err){
        res.json({success: false, message: 'Что-то пошло не так'});
      } else {
        if (!user){
          res.json({success: false, message: 'Пользователь не найден'});
        } else{
          if (!user.check) {
            res.json({success: false});
          } else {
            res.json({success: true})
          }
        }
      }
    })
  });

  router.get('/editLogin/:id', (req,res) => {

    User.find({_id: req.params.id},(err, user) =>{
      if (err){
        res.json({success: false, message: 'Что-то пошло не так'});
      } else {
        if (!user){
          res.json({success: false, message: 'Пользователь не найден'});
        } else{
          if (!user.check) {
            user.check = true;
            res.json({success: true, message: 'E-mail подтвержден'});
          }
        }
      }
    })
  });

  return router;
};
