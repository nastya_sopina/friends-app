const express = require('express');
const app = express();
const router = express.Router();

const mongoose = require('mongoose');
const config = require('./config/database');
const path = require('path');
const auth = require('./routes/auth')(router);
const publications = require('./routes/publications')(router);
const bodyparser = require('body-parser');
const cors = require('cors');
//const port = process.env.PORT || 8080

mongoose.Promise = global.Promise;
mongoose.connect(config.uri, (err) =>{
  if(err){
    console.log('Could not connect to db:', err);
  } else{
    console.log('Connect to db');
  }
});

app.use(cors({
  origin: 'http://localhost:4200'
}));

app.use(bodyparser.urlencoded({extended:false}))
app.use(bodyparser.json());

app.use(express.static(__dirname + '/../dist'));
app.use('/authentication', auth);
app.use('/publications', publications);

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/../dist/index.html'));
});

app.listen(8080,()=>{
  console.log('listening on port 8080')
});
