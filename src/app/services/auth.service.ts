import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {tokenNotExpired} from 'angular2-jwt';

@Injectable()
export class AuthService {

  domain = 'http://localhost:8080/';
  authToken;
  user;
  options;

  constructor(
    private http: Http

  ) { }

  createAuthenticationHeaders() {
        this.loadToken();
          this.options = new RequestOptions({
            headers: new Headers({
             'Content-Type': 'application/json', // Format set to JSON
              'authorization': this.authToken // Attach token
          })
        });
  }

  checkLogin(id){
    return this.http.get(this.domain + 'authentication/checkLogin/' + id).map( res => res.json());
  }

  editLogin(id) {
    return this.http.get(this.domain + 'authentication/editLogin/' + id).map( res => res.json());
  }

  loadToken() {
    this.authToken = localStorage.getItem('token');
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  register(user) {
      return this.http.post(this.domain + 'authentication/register', user).map(res => res.json());
  }

  checkUsername(username) {
    return this.http.get(this.domain + 'authentication/checkUsername/' + username).map(res => res.json());
  }

  checkEmail(email) {
        return this.http.get(this.domain + 'authentication/checkEmail/' + email).map(res => res.json());
  }

  login(user) {
    return this.http.post(this.domain + 'authentication/login/', user).map(res => res.json());
  }

  login2(user) {
    return this.http.post(this.domain + 'authentication/login2/', user).map(res => res.json());
  }

  storeUserData(token, user) {
        localStorage.setItem('token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
  }

  getProfiles(username) {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'authentication/profiles/' + username, this.options).map(res => res.json());
  }

  getProfile() {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'authentication/profile', this.options).map(res => res.json());
  }

  getProfile2(id) {
    return this.http.get(this.domain + 'authentication/profil/' + id).map(res => res.json());
  }

  editOverview(user, id) {
    this.createAuthenticationHeaders();
    return this.http.put(this.domain + 'authentication/editOverview/' + id, user , this.options).map(res => (res.json()));
  }

  isLoggedIn() {
    return tokenNotExpired();
  }

  getAllProfiles() {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'authentication/allProfiles', this.options).map(res => res.json());
  }

  searchUsers(text) {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'authentication/searchprofile/' + text, this.options).map(res => res.json());
  }

  addPeople( user: string, username: string) {
    const postData = {
      user: user,
      username: username
    };

    this.createAuthenticationHeaders();
    return this.http.post(this.domain + 'authentication/addFriends', postData, this.options).map(res => (res.json()));
  }

}
