import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {AuthService} from './auth.service';

@Injectable()
export class PostService {

  domain = this.authService.domain;
  options;

  constructor(
    private http: Http,
    private authService: AuthService

  ) { }

  createAuthenticationHeaders() {
    this.authService.loadToken();
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'authorization': this.authService.authToken
      })
    });
  }

  newPosts(publication) {
    this.createAuthenticationHeaders();
    return this.http.post(this.domain + 'publications/newPublication', publication , this.options).map(res => (res.json()));
  }

  getAllPosts() {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'publications/getAllPublications', this.options).map(res => (res.json()));
  }

  getPost(id) {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'publications/editPost/' + id, this.options).map(res => (res.json()));
  }

  updatePost(publication) {
    this.createAuthenticationHeaders();
    return this.http.put(this.domain + 'publications/updatePost/', publication , this.options).map(res => (res.json()));
  }

  deletePost(id) {
    this.createAuthenticationHeaders();
    return this.http.delete(this.domain + 'publications/deletePost/' + id, this.options).map(res => (res.json()));
  }

  postComment(id, comment) {
    this.createAuthenticationHeaders();
    const postData = {
      id: id,
      comment: comment
    };
    return this.http.post(this.domain + 'publications/comment/', postData, this.options).map(res => (res.json()));
  }
}


