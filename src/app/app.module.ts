import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing.model';
import {SettingsModule} from './components/settings/settings.module';
import { HttpModule } from '@angular/http';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {AppComponent} from './app.component';
import {HomepageComponent} from './components/homepage/homepage.component';
import {NavsComponent} from './components/navs/navs.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {LoginComponent} from './components/login/login.component';
import {PostsComponent} from './components/posts/posts.component';
import {EditPostComponent} from './components/posts/edit-post/edit-post.component';
import {DeletePostComponent} from './components/posts/delete-post/delete-post.component';
import {AddPostsComponent} from './components/posts/add-posts/add-posts.component';
import {NotfoundComponent} from './components/notfound/notfound.component';
import {ProfileComponent} from './components/profile/profile.component';
import {PreviewMypostComponent} from './components/posts/preview-mypost/preview-mypost.component';
import {AuthService} from './services/auth.service';
import {PostService} from './services/post.service';
import {AuthGuard} from './guards/auth.guard';
import {NotAuthGuard} from './guards/notauth.guard';
import { PeopleComponent } from './components/people/people.component';



@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    NavsComponent,
    RegistrationComponent,
    LoginComponent,
    PostsComponent,
    EditPostComponent,
    DeletePostComponent,
    NotfoundComponent,
    ProfileComponent,
    PreviewMypostComponent,
    AddPostsComponent,
    PeopleComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    SettingsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [AuthService, PostService, AuthGuard, NotAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
