import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';


@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css', '../../app.component.css']
})
export class PeopleComponent implements OnInit {

  users;
  serchStr = '';
  photo = '../../../assets/images/no_avatar.jpg';
  curentUrl;


  constructor(
    private authService: AuthService,
    private activateRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.curentUrl = this.activateRoute.snapshot.params;
  }

  handleChange() {
    if (this.serchStr) {
      this.authService.searchUsers(this.serchStr.toLowerCase())
        .subscribe(data => {
          this.users = data.users;
        });
    } else {
      this.authService.getAllProfiles().subscribe((data) => {
        this.users = data.users;
      });
    }
  }

  ngOnInit() {

    this.curentUrl = this.activateRoute.snapshot.params;
    if (this.curentUrl.id) {
      this.authService.getProfiles(this.curentUrl.id).subscribe((profile) => {
        if (profile.success) {
         // console.log(profile.user.friends);
          this.users = profile.user.friends;
        }
      });
    } else {
      this.authService.getAllProfiles().subscribe((data) => {
        this.users = data.users;
      });
    }
  }
}

