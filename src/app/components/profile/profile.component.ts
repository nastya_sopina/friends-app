import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css', '../../app.component.css']
})
export class ProfileComponent implements OnInit {

  user = {
    _id: '',
    username: '',
    email: '',
    fullname: ''
  };
  username;
  onnuser;
  curentUrl;
  myuser = {
    username: '',
    friends: [],
  };
  messageClass;
  message;
  showpublication = false;


  constructor(
    private authService: AuthService,
    private activateRoute: ActivatedRoute,
  ) {
  }

  Subscribe(username) {
    this.authService.addPeople(this.myuser.username, username).subscribe((data) => {
      if (!data.success) {
        this.messageClass = 'alert alert-danger';
        this.message = data.message;
      } else {
        this.authService.getProfile().subscribe(myprofile => {
          if (myprofile.success) {
            this.messageClass = 'alert alert-success';
            this.message = data.message;

            this.myuser = myprofile.user;
            this.username = this.user.username;
            if (this.myuser.username) {
              for (let j = 0 ; j < this.myuser.friends.length; j++) {
                if (this.myuser.friends[j].username === this.username) {
                  this.onnuser = true;
                  break;
                } else {
                  this.onnuser = false;
                }
              }
            }

            setTimeout(() => {
              this.message = false;
            }, 1000);
          }
        });
      }
    });
  }

  showPublication() {
    if (this.showpublication) {
      this.showpublication = false;
    } else {
      this.showpublication = true;
    }
  }

  ngOnInit() {

    this.curentUrl = this.activateRoute.snapshot.params;
    if (this.curentUrl.id) {
      this.authService.getProfiles(this.curentUrl.id).subscribe((profile) => {
        if (profile.success) {
          this.authService.getProfile().subscribe(myprofile => {
            if (myprofile.success) {
              this.myuser = myprofile.user;
              this.user = profile.user;
              this.username = profile.user.username;
              if (this.myuser.username) {
                for (let j = 0 ; j < this.myuser.friends.length; j++) {
                  if (this.myuser.friends[j].username === this.username) {
                    this.onnuser = true;
                    break;
                  } else {
                    this.onnuser = false;
                  }
                }
              }
            }
          });
        }
      });
    } else {
      this.authService.getProfile().subscribe((profile) => {
        if (profile.success) {
          this.myuser = profile.user;
          this.user = profile.user;
        }
      });
    }
  }

}
