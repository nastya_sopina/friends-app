import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  regform: FormGroup;
  text;
  message;
  messageClass;
  processing = false;
  emailValid;
  emailMessage;
  usernameValid;
  usernameMessage;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {
    this.createForm();
  }


  createForm() {
    this.regform = this.formBuilder.group({

      firstname: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ])],
      secondname: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50),
        this.validateEmail
      ])],
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(20),
        this.validateUsername
      ])],

      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(35),
      ])],

      confirm: ['', Validators.required]
    }, { validator: this.matchingPasswords('password', 'confirm') });
  }

  disableForm() {
    this.regform.controls['firstname'].disable();
    this.regform.controls['secondname'].disable();
    this.regform.controls['email'].disable();
    this.regform.controls['username'].disable();
    this.regform.controls['password'].disable();
    this.regform.controls['confirm'].disable();
  }

  enableForm() {
    this.regform.controls['firstname'].enable();
    this.regform.controls['secondname'].enable();
    this.regform.controls['email'].enable();
    this.regform.controls['username'].enable();
    this.regform.controls['password'].enable();
    this.regform.controls['confirm'].enable();
  }


  validateEmail(controls) {

    const regExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    if (regExp.test(controls.value)) {
      return null;
    } else {
      return {
        'validateEmail': true
      };
    }
  }

  validateUsername(controls) {

    const regExp = new RegExp(/^[a-zA-Z0-9]+$/);

    if (regExp.test(controls.value)) {
      return null;
    } else {
      return {
        'validateUsername': true
      };
    }
  }

  matchingPasswords(password, confirm) {
    return (group: FormGroup) => {

      if (group.controls[password].value === group.controls[confirm].value) {
        return null;
      } else {
        return {
          'matchingPasswords': true
        };
      }
    };
  }

  onRegisterSubmit() {

    this.processing = true;
    this.disableForm();
    const user = {
      firstname: this.regform.get('firstname').value,
      secondname: this.regform.get('secondname').value,
      email: this.regform.get('email').value,
      username: this.regform.get('username').value,
      password: this.regform.get('password').value
    };

    this.auth.register(user).subscribe(data => {
      if (!data.success) {
        this.messageClass = 'alert alert-danger';
        this.message = data.message;
        this.processing = false;
        this.enableForm();
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        setTimeout(() => {
                      this.router.navigate(['/login']);
                    }, 2000);
      }
    });
  }

  checkEmail() {

        this.auth.checkEmail(this.regform.get('email').value).subscribe(data => {

              if (!data.success) {
                this.emailValid = false;
                this.emailMessage = data.message;
              } else {
                this.emailValid = true;
                this.emailMessage = data.message;
              }
          });
    }

  checkUsername() {
      // Function from authentication file to check if username is taken
        this.auth.checkUsername(this.regform.get('username').value).subscribe(data => {
            // Check if success true or success false was returned from API
              if (!data.success) {
                this.usernameValid = false; // Return username as invalid
                this.usernameMessage = data.message; // Return error message
              } else {
                this.usernameValid = true; // Return username as valid
                this.usernameMessage = data.message; // Return success message
              }
          });
  }

  ngOnInit() {
  }

}
