import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css', '../../app.component.css']
})
export class SettingsComponent implements OnInit {

  user = {
    username: null,
  };
  constructor() { }

  ngOnInit() {
  }

}
