import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-overview-settings',
  templateUrl: './overview-settings.component.html',
  styleUrls: ['./overview-settings.component.css']
})
export class OverviewSettingsComponent implements OnInit {

  user = {
    username: null,
    phonenumber: null,
    country: null,
    city: null,
  };
  messageClass;
  message;
  processing = false;
  form: FormGroup;
  curentUrl;


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private activateRoute: ActivatedRoute,
  ) {
    this.createForm();
  }

  createForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      phonenumber: ['', Validators.required],
      country: ['', Validators.required],
      city: ['', Validators.required],
    });

  }

  ngOnInit() {
    this.curentUrl = this.activateRoute.snapshot.params;
    this.authService.getProfile().subscribe((profile) => {
      this.user = profile.user;
    });
  }

  ContactSubmit() {

     this.processing = true;
     this.disableForm();

    if (this.form.get('username').value) {
      this.user.username = this.form.get('username').value;
    }
    if (this.form.get('phonenumber').value) {
      this.user.phonenumber = this.form.get('phonenumber').value;
    }
      if (this.form.get('country').value) {
        this.user.country = this.form.get('country').value;
      }
      if (this.form.get('city').value) {
        this.user.city = this.form.get('city').value;
      }

    this.authService.editOverview(this.user, this.curentUrl).subscribe(data => {
      if (data.success === false) {
        this.messageClass = 'alert alert-danger';
        this.message = data.message;
        this.processing = false;
        this.enableForm();
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        setTimeout(() => {
          this.router.navigate(['/profile']);
        }, 1000);
      }
    });
  }

  disableForm() {
      this.form.controls['username'].disable();
     this.form.controls['phonenumber'].disable();
     this.form.controls['city'].disable();
     this.form.controls['country'].disable();
  }

  enableForm() {
      this.form.controls['username'].enable();
      this.form.controls['phonenumber'].enable();
      this.form.controls['city'].enable();
      this.form.controls['country'].enable();
  }


}
