import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-navs',
  templateUrl: './navs.component.html',
  styleUrls: ['./navs.component.css']
})
export class NavsComponent implements OnInit {

  user = {
    username: '',
  };

  constructor(
    private authService: AuthService,
    private router: Router,
  //  private flashmessage: FlashMessage
  ) {
this.ngOnInit();
  }

  onlogout() {

    this.authService.logout();
  //  this.flashmessage.info('You are logged out');
    this.router.navigate(['/']);
  }

  ngOnInit() {

    this.authService.getProfile().subscribe((profile) => {
      if ( profile.success) {
        this.user = profile.user;
      }
    });
  }

}
