import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {AuthGuard} from '../../guards/auth.guard';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  messageClass = '';
  message;
  processing = false;
  form: FormGroup;
  formauth: FormGroup;
  previousUrl;
  check = false;
  profile;
  id;
  user;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private authGuard: AuthGuard
  ) {
    this.createForm();
    this.createFormAuth();
  }

  createForm() {
    this.form = this.formBuilder.group({
      password: ['', Validators.required],
      email: ['', Validators.required]
    });
  }

  createFormAuth() {
    this.formauth = this.formBuilder.group({
      id: ['', Validators.required]
    });
  }

  disableForm() {
    this.form.controls['email'].disable();
    this.form.controls['password'].disable();
  }

  enableForm() {
    this.form.controls['email'].enable();
    this.form.controls['password'].enable();
  }

  disableAuthForm() {
    this.formauth.controls['id'].disable();
  }

  enableAuthForm() {
    this.formauth.controls['id'].enable();
  }

  onLoginSubmit() {
    this.processing = true;
    this.disableForm();
    this.user = {
        password: this.form.get('password').value,
        email: this.form.get('email').value,
      };

      this.authService.login2(this.user).subscribe(data => {
        if (!data.success) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
          this.processing = false;
          this.enableForm();
        } else {
          this.profile = data;
          if (!data.user.check) {
            this.check = true;
            this.messageClass = 'alert alert-danger';
            this.message = 'E-mail не подтвержден';
            setTimeout(() => {
              this.messageClass = '' ;
            }, 1000);
          } else {
            this.authService.storeUserData(data.token, data.user);

            this.messageClass = 'alert alert-success';
            this.message = data.message;
            setTimeout(() => {
              if (this.previousUrl) {
                this.router.navigate([this.previousUrl]);
              } else {
                this.router.navigate(['']);
              }
            }, 1000);
          }
        }
      });
  }

  onAuthSubmit() {

    this.disableAuthForm();
    this.processing = true;
    this.user = {
      password: this.form.get('password').value,
      email: this.form.get('email').value,
      id: this.formauth.get('id').value,
    };
    this.authService.login2(this.user).subscribe(data => {
      if (!data.success) {
        console.log('tut1');
        this.messageClass = 'alert alert-danger';
        this.message = 'Не верный код подтверждения';
        this.processing = false;
        this.enableAuthForm();
        setTimeout(() => {
          this.messageClass = '' ;
        }, 1000);
      } else {
        if (!data.user.check) {
          this.check = true;
          this.messageClass = 'alert alert-danger';
          this.message = 'E-mail не подтвержден';
        } else {
          this.authService.storeUserData(data.token, data.user);
          this.messageClass = 'alert alert-success';
          this.message = data.message;
          setTimeout(() => {
            if (this.previousUrl) {
              this.router.navigate([this.previousUrl]);
            } else {
              this.router.navigate(['']);
            }
          }, 1000);
        }
      }
    });
  }

  ngOnInit() {
    if (this.authGuard.redirectUrl) {
      this.messageClass = 'alert alert-danger';
      this.message = 'You mast be logger in to view that page.';
      this.previousUrl = this.authGuard.redirectUrl;
      this.authGuard.redirectUrl = undefined;
    }
  }

}
