import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {PostService} from '../../../services/post.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';


@Component({
  selector: 'app-add-posts',
  templateUrl: './add-posts.component.html',
  styleUrls: ['./add-posts.component.css', '../../../app.component.css']
})
export class AddPostsComponent implements OnInit {

  form: FormGroup;
  processing = false;
  newPublication = false;
  username;
  messageClass;
  message;
  text: String;
  user: String;
  photo;
  fullname;



  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private postService: PostService,
    private location: Location,
    private router: Router,
  ) {
    this.createPublicationForm();
  }

  createPublicationForm() {
    this.form = this.formBuilder.group(({
      text: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(1000),
        Validators.minLength(5),
      ])]
    }));
  }

  onPublicationSubmit() {

    this.processing = true;
    this.disablePublicationform();

    const publication = {
      text: this.form.get('text').value,
      user: this.username,
      photo: this.photo,
      username: this.fullname,
    };


    this.postService.newPosts(publication).subscribe(data => {
      if (!data.success) {
        this.messageClass = 'alert alert-danger';
        this.message = data.message;
        this.processing = false;
        this.enablePublicationform();
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        setTimeout(() => {
          this.newPublication = false;
          this.processing = false;
          this.message = false;
          this.form.reset();
          this.enablePublicationform();
          this.back();
        }, 1000 );
      }
    });
  }

  back() {
    this.location.back();
  }


  disablePublicationform() {
    this.form.get('text').disable();
  }

  enablePublicationform() {
    this.form.get('text').enable();
  }

  ngOnInit() {

    this.authService.getProfile().subscribe(profile => {
      this.username = profile.user.username;
      this.photo = profile.user.photo;
      this.fullname = profile.user.fullname;
    });
  }

}

