import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {PostService} from '../../../services/post.service';

@Component({
  selector: 'app-delete-post',
  templateUrl: './delete-post.component.html',
  styleUrls: ['./delete-post.component.css', '../../../app.component.css']
})
export class DeletePostComponent implements OnInit {


  processing = false;
  messageClass;
  message;
  foundBlog = true;
  publication = {
    text: '',
    user : '',
    createdAt : '',
    photo : '',
  };
  curentUrl;


  constructor(
    private activateRoute: ActivatedRoute,
    private postService: PostService,
    private location: Location,
    private router: Router) {
  }

  deletePost() {
    this.processing = true;
    this.postService.deletePost(this.curentUrl.id).subscribe( data => {
      if (!data.success) {
        this.messageClass = 'alert alert-danger';
        this.message = 'Blog not deleted';
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        setTimeout(() => {
          this.back();
        }, 1000);
      }
    });
  }

  back() {
    this.location.back();
  }

  ngOnInit() {

    this.curentUrl = this.activateRoute.snapshot.params;
    this.postService.getPost(this.curentUrl.id).subscribe(data => {
      if (data.success === false) {
        this.messageClass = 'alert alert-danger';
        this.message = 'Blog not found';
      } else {
        this.publication = {
          text: data.publication.text,
          user : data.publication.user,
          createdAt : data.publication.createdAt,
          photo : data.publication.photo,
        };
      }
    });

  }

}
