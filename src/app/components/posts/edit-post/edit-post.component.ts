import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {PostService} from '../../../services/post.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css', '../../../app.component.css']
})
export class EditPostComponent implements OnInit {

  processing = false;
  loading = true;
  messageClass;
  message;
  publication;
  curentUrl;


  constructor(
    private activateRoute: ActivatedRoute,
    private postService: PostService,
    private location: Location,
    private router: Router) {
  }

  back() {
    this.location.back();
  }

  ngOnInit() {
    this.curentUrl = this.activateRoute.snapshot.params;
    this.postService.getPost(this.curentUrl.id).subscribe(data => {
      if (data.success === false) {
        this.messageClass = 'alert alert-danger';
        this.message = 'Публикация не найдена';
      } else {
        this.publication = data.publication;
        this.loading = false;
      }
    });
  }

  editPostSubmit() {

    this.processing = true;
    this.postService.updatePost(this.publication).subscribe(data => {
      if (data.success === false) {
        this.messageClass = 'alert alert-danger';
        this.message = data.message;
        this.processing = false;
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        setTimeout(() => {
          this.back();
        }, 1000);
      }
    });
  }

}
