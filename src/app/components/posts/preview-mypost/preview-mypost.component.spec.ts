import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewMypostComponent } from './preview-mypost.component';

describe('PreviewMypostComponent', () => {
  let component: PreviewMypostComponent;
  let fixture: ComponentFixture<PreviewMypostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewMypostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewMypostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
