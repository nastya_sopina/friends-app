import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {PostService} from '../../../services/post.service';

@Component({
  selector: 'app-preview-mypost',
  templateUrl: './preview-mypost.component.html',
  styleUrls: ['./preview-mypost.component.css', '../../../app.component.css']
})
export class PreviewMypostComponent implements OnInit {

  form: FormGroup;
  commentForm;
  processing = false;
  newPublication = false;
  username;
  messageClass;
  message;
  text: String;
  user: String;
  myposts;
  newComment = [];
  enabledComments = [];
  curentUrl;


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private activateRoute: ActivatedRoute,
    private postService: PostService
  ) {
    this.createPublicationForm();
    this.createCommentForm();
    this.ngOnInit();
  }

  createPublicationForm() {
    this.form = this.formBuilder.group(({
      text: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(1000),
        Validators.minLength(5),
      ])]
    }));
  }

  createCommentForm() {
    this.commentForm = this.formBuilder.group(({
      comment: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(200),
        Validators.minLength(1),
      ])]
    }));
  }

  getMyPosts() {
    this.postService.getAllPosts().subscribe(data => {
      this.myposts = data.publications;
      for (let i = 0; i < this.myposts.length; i++) {
        if (this.myposts[i].user !== this.username) {
          this.myposts.splice(i, 1);
          i--;
        }
      }
    });
  }

  draftComment(id) {
    this.commentForm.reset();
    this.newComment = [];
    this.newComment.push(id);
  }

  cancelSubmission(id) {
    const index = this.newComment.indexOf(id);
    this.newComment.splice(index, 1);
    this.commentForm.reset();
    this.enableCommentForm();
    this.processing = false;
  }

  postComment(id) {
    this.disableCommentForm();
    this.processing = true;
    const comment = this.commentForm.get('comment').value;
    this.postService.postComment(id, comment).subscribe(data => {
      this.getMyPosts();
      const index = this.newComment.indexOf(id);
      this.enableCommentForm();
      this.commentForm.reset();
      this.processing = false;
      this.cancelSubmission(id);
      if (this.enabledComments.indexOf(id) < 0) {
        this.expand(id);
      }
    });
  }

  expand(id) {
    this.enabledComments.push(id);
  }

  collapse(id) {
    const index = this.enabledComments.indexOf(id);
    this.enabledComments.splice(index, 1);
  }

  enableCommentForm() {
    this.commentForm.get('comment').enable();
  }

  disableCommentForm() {
    this.commentForm.get('comment').disable();
  }

  ngOnInit() {

    this.curentUrl = this.activateRoute.snapshot.params;
    if (this.curentUrl.id) {
      this.authService.getProfiles(this.curentUrl.id).subscribe(profile => {
        if (profile.success) {
          this.username = profile.user.username;
        }
      });
    } else {
      this.authService.getProfile().subscribe(profile => {
        if (profile.success) {
          this.username = profile.user.username;
        }
      });
    }
    this.getMyPosts();
  }

}
