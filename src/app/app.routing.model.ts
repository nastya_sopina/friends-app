import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {NotAuthGuard} from './guards/notauth.guard';

import {NgModule} from '@angular/core';
import {HomepageComponent} from './components/homepage/homepage.component';
import {ProfileComponent} from './components/profile/profile.component';
import {SettingsComponent} from './components/settings/settings.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {LoginComponent} from './components/login/login.component';
import {PostsComponent} from './components/posts/posts.component';
import {EditPostComponent} from './components/posts/edit-post/edit-post.component';
import {DeletePostComponent} from './components/posts/delete-post/delete-post.component';
import {AddPostsComponent} from './components/posts/add-posts/add-posts.component';
import {NotfoundComponent} from './components/notfound/notfound.component';
import {PeopleComponent} from './components/people/people.component';


const AppRoutes: Routes = [

  {
    path: '',
    component: HomepageComponent,
  },
  {
    path: 'profile',
    component :  ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile/:id',
    component :  ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'people',
    component : PeopleComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'settings',
    component : SettingsComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'friends/:id',
    component : PeopleComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'registration',
    component : RegistrationComponent,
    canActivate : [NotAuthGuard]
  },
  {
    path: 'login',
    component : LoginComponent,
    canActivate: [NotAuthGuard]
  },
  {
    path: 'publications',
    component : PostsComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'edit-post/:id',
    component : EditPostComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'delete-post/:id',
    component : DeletePostComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'new-post',
    component : AddPostsComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'people',
    component : PeopleComponent,
    canActivate : [AuthGuard]
  },
  {
    path: '**',
    component : NotfoundComponent,
  }
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(AppRoutes)],
  providers: [],
  bootstrap: [],
  exports: [RouterModule]
})

export class AppRoutingModule { }
